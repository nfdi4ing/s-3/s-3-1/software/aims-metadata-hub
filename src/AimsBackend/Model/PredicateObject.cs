﻿namespace AimsBackend.Model
{
    public class PredicateObject
    {
        public string Predicate { get; set; } = null!;
        public string Object { get; set; } = null!;
    }
}
