﻿namespace AimsBackend.Model
{
    public class ApplicationProfileObject
    {
        public string Name { get; set; } = "";
        public string BaseUrl { get; set; } = "";
        public string Definition { get; set; } = "";
        public string MimeType { get; set; } = "";
    }
}
