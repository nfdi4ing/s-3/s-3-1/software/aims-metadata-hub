## AIMS Metadata Service

[[_TOC_]] 

## 📝 Overview

The AIMS Metadata Service is a REST API that provides a service for storing metadata in the AIMS platform.

Deployment location: https://api.aims.otc.coscine.dev/

## ⚙️ Configuration

Before you can run and use the API, you need to ensure that the following dependencies and prerequisites are in place:

1. A Coscine database instance must be running. If you haven't set up a Coscine database instance yet, please refer to the [Coscine documentation]() for instructions on how to do so.
2. The Consul service should be correctly set up as a Key-Value store. If you haven't set up Consul yet, please refer to the [Consul documentation]() for instructions on how to do so.
3. The project's referenced .NET SDK(s) must be installed. Please refer to the project's source code for information on which .NET SDK(s) are required.

Once you have all the necessary dependencies and prerequisites in place, you should be able to run and use the API with the appropriate authentication credentials.

## 📚 API Reference

To get started with this project, you will need to ensure you have configured it correctly. Please refer to the configuration guide above. Once the project is running, you can access the API through the following endpoint:
```xml
https://api.aims.otc.coscine.dev/
```

To authenticate in Swagger with a Bearer JWT token, you will need to generate a token in Coscine under your _User Profile_. Once you have your token, you can open the Swagger documentation for the API you want to authenticate with and click on the "Authorize" button. In the "Authorization Value" field, you should enter `Bearer` followed by a space and then your Bearer JWT token. Click on the "Authorize" button to authenticate, and you should now be able to make authenticated requests to the API. 
> :bulb: Make sure to keep your Bearer JWT token secure, as it grants access to your account and API resources. If you suspect that your token has been compromised, you should generate a new one and invalidate all existing ones immediately.

For detailed information about each endpoint and the parameters they accept, please refer to the project's source code.

## 👥 Contributing

As an open source plattform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!
